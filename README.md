# README #

See the LiteMaps Engine for details on this project.

### What is this repository for? ###

This repo should be used to grab all the source for LiteMaps to demo and explore its capabilities.

### How do I get set up? ###

* Checkout this solution repository and demo one of the Desktop UI projects to see how everything works.
* Write your own TileImageSource
* Write your own UI implementation
* Integrate both into your own application

### Who do I talk to? ###

* Contact me if there are any issues or bugs
