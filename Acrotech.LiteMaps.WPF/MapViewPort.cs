﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Acrotech.LiteMaps.Engine;
using Acrotech.LiteMaps.Engine.Sources;
using NLog;

namespace Acrotech.LiteMaps.WPF
{
    public class MapViewPort : Panel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly Brush DebugTextBackgroundBrush = new SolidColorBrush(Color.FromArgb(100, 100, 100, 100));
        private static readonly Pen GridPen = new Pen(Brushes.White, 1d);
        public static readonly Typeface DebugTypeface = new Typeface("Courier New");

        private static readonly TaskScheduler UITaskScheduler = TaskScheduler.FromCurrentSynchronizationContext();

        public MapViewPort()
        {
            DataContextChanged += (s, e) => Initialize();

            Images = new Dictionary<string, ImageSource>();
            WaitHandle = new ManualResetEventSlim(false);
        }

        public ViewPortViewModel ViewModel { get { return (ViewPortViewModel)DataContext; } }

        public Dictionary<string, ImageSource> Images { get; private set; }

        private ManualResetEventSlim WaitHandle { get; set; }

        private void Initialize()
        {
            if (ViewModel != null)
            {
                ViewModel.PropertyChanged += ViewModel_PropertyChanged;
                TileImageSource.Downloader.TileDownloaded += (s, t) => RefreshMapAsync();

                OnRenderSizeChanged(null);

                var viewModel = ViewModel;
                ThreadPool.QueueUserWorkItem(_ => ImageLoader(viewModel));
            }
        }

        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Tiles")
            {
                RefreshMapAsync();
            }
        }

        public Task RefreshMapAsync()
        {
            return Task.Factory.StartNew(RefreshMap, CancellationToken.None, TaskCreationOptions.None, UITaskScheduler);
        }

        public void RefreshMap()
        {
            InvalidateVisual();
        }

        protected override void OnRenderSizeChanged(System.Windows.SizeChangedInfo sizeInfo)
        {
            if (sizeInfo != null)
            {
                base.OnRenderSizeChanged(sizeInfo);
            }

            if (RenderSize.IsEmpty == false && ViewModel != null)
            {
                ViewModel.Resize((int)RenderSize.Width, (int)RenderSize.Height);
            }
        }

        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);

            if (ViewModel != null)
            {
                if (RenderSize.IsEmpty == false)
                {
                    dc.DrawRectangle(Brushes.Black, null, new Rect(0, 0, RenderSize.Width, RenderSize.Height));

                    var tiles = ViewModel.Tiles;

                    if (tiles != null)
                    {
                        var tileSize = ViewModel.Map.TileImageSource.TileSize;

                        ImageSource image = null;

                        foreach (var tile in tiles)
                        {
                            var bounds = new Rect(tile.X + 0.5, tile.Y + 0.5, tileSize, tileSize);

                            if (Images.TryGetValue(tile.Tile.Key, out image))
                            {
                                dc.DrawImage(image, bounds);
                            }

                            if (ViewModel.IsTileGridVisible)
                            {
                                dc.DrawRectangle(null, GridPen, bounds);
                            }

                            if (ViewModel.IsTileDebugTextVisible)
                            {
                                dc.DrawRectangle(DebugTextBackgroundBrush, null, bounds);

                                var txt = new FormattedText(tile.ToString(), CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, DebugTypeface, 12, Brushes.White)
                                {
                                    MaxTextWidth = tileSize,
                                    MaxTextHeight = tileSize,
                                    TextAlignment = TextAlignment.Center
                                };

                                var p = new Point(bounds.X, bounds.Y + tileSize / 2 - txt.Height / 2);

                                dc.DrawText(txt, p);
                            }
                        }
                    }

                    if (ViewModel.IsCrossVisible)
                    {
                        var x = RenderSize.Width / 2 + 0.5;
                        var y = RenderSize.Height / 2 + 0.5;

                        dc.DrawLine(GridPen, new Point(x, 0), new Point(x, RenderSize.Height));
                        dc.DrawLine(GridPen, new Point(0, y), new Point(RenderSize.Width, y));
                    }
                }
            }
        }

        private void ImageLoader(ViewPortViewModel viewModel)
        {
            while (WaitHandle.Wait(0) == false)
            {
                var isUpdated = viewModel.UpdateImages(Images, CreateFrozenImageSource);

                if (isUpdated == false)
                {
                    WaitHandle.Wait(100);
                }
                else
                {
                    RefreshMapAsync();
                }
            }
        }

        private ImageSource CreateFrozenImageSource(Uri uri)
        {
            var image = new BitmapImage(uri);

            image.Freeze();

            return image;
        }

        #region Mouse Handling

        private Point DragOrigin { get; set; }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            DragOrigin = e.GetPosition(this);

            CaptureMouse();
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            ReleaseMouseCapture();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (IsMouseCaptured)
            {
                var v = DragOrigin - e.GetPosition(this);

                ViewModel.Pan((int)v.X, (int)v.Y);

                DragOrigin = e.GetPosition(this);
            }
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                if (ViewModel.Map.ZoomIn.CanExecute())
                {
                    ViewModel.Map.ZoomIn.Execute();
                }
            }
            else if (e.Delta < 0)
            {
                if (ViewModel.Map.ZoomOut.CanExecute())
                {
                    ViewModel.Map.ZoomOut.Execute();
                }
            }
        }

        #endregion
    }
}
